# User Testing Workshop 2021-05-15 Narrm

## Structure

- acknowledgment of country
- quick chat about who is funding this workshop and who magma are
- icebreaker games
    - Tabletop game with keys
        - layout keys on table on top of blank card
        - describe scenario where you have to leave your house for a week and you want to leave access for others to feed plants/pets/etc
        - ask each person what their strategy with the keys is
    - Digital files access for others
        - ask others if they have an equivalent process for their digital files
![](https://i.imgur.com/VrhpGy1.jpg)
![](https://i.imgur.com/OS1Llp6.jpg)


- quick intro to briar
- quick intro to P2P tech vs. centralised tech
- start testing briar
    - setup
        - get each person to connect to the internet
        - get each person to set up their account on briar
        - get each person to connect with eachother
        - get each person to message eachother about the previous exercises or to send any message of their own liking
    - scenario one: a person loses their phone without social backup
        - remove a phone from someone and wipe their data
        - get them to setup an account again
        - pause and talk about what lose of data means in p2p systems
        - conclude that all those messages are lost to the loser
    - scernario two: a person loses their phone having backed up socially
        - get everyone to do social backups
        - initial loser nominates another person from within the group to lose their phone
        - proceed to recover account
![](https://i.imgur.com/nRuJA7l.jpg)
![](https://i.imgur.com/rzJ8sSo.jpg)
![](https://i.imgur.com/JfPbB8P.jpg)
![](https://i.imgur.com/MFg715N.jpg)
![](https://i.imgur.com/wx1Cprj.jpg)
![](https://i.imgur.com/cRXcGjI.jpg)
![](https://i.imgur.com/0QuLraB.jpg)

## Notes from the session

:::info
> [name=dan]
> these notes are pulled from my notebook and i haven't yet structured
:::

- La
    - would be good to know before going through the process what the process for recovering the data will be
        - this would effect who you choose as secret holders e.g. people with whome it is easy to meet up physically with
    - unclear what is happening within the quorum page. unclear why you can't select some values. feels like choices are being taken away without any information about why (frustrating)
- R
    - like the name shard
    - horcrux is a useful analogy
    - likes the name burger for menu item
    - was not able to make a social backup as screen got stuck and wouldn't allow the tick to go through (silent failure)
    - could not become friends with new Le until a phone restart
- W
    - really unclear about process
- Le
    - process confusing

- DH
    - Group would not have thought to look in settings for manually backing up
    - Group would not have thought to look in their messages to be able to help someone recover their backup
    - Many in group tried to 'Add contact' when recovering a contact

