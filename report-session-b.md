
## Social Backup User testing session B

- 5 Participants joined the session.
- The session was given in English.  All 5 were competent English speakers but none had English as their first language.Participant's native languages were German, Spanish and Arabic. Care was taken to be aware of language issues affecting comprehension of the tasks and usability issues.
- All 5 regularly used Android on their personal devices.
- All 5 had experience using messaging apps popular in western countries (One or more of Telegram, Signal, Whatsapp)
- 2 participants had worked in 'technical' jobs in the last 5 years
- 2 participants had an academic background - one had a Batchelor of arts, and one a Master of science 
- None used encrypted email regularly - an indicator of familiarity with concepts involving cryptographic keys
- None were familiar with Shamirs secret sharing. One was familiar with facebook's social recovery feature but had not used it.

## Terminology

In this report we use the terms 'trusted contact' and 'custodian' interchangably. Similarly the terms 'backup piece' and 'shard'. During the session, the specialist terms 'custodian' and 'shard' were never used.  Care was taken to use the same terminology used in the user interface of the feature.

## Ice-breaker activity

To get to know each other and begin to think about trust, participants were asked to imagine they were going away and leave their home empty, and describe who they might give their spare key to.

## Test process

Participants ran through the backup and recovery process several times, taking different roles (either secret-owner or custodian). Additionally, the feature was tested using a tablet rather than a smartphone, which has a different display resolution and hardware features, to see how this effected the functionality and UI.

## UI Issues

- All users were able to find the 'Social backup' settings option when asked to backup their account.

- Two users said the 'Help recover account' option was difficult to find (although all were able to find it).

- Interestingly, the two users with experience in technical jobs took the longest time to find both of these options.

![screenshot backup pieces sent](./img/Screenshot_backup-pieces-sent-sm.png)

- The 'Got it' button on the confirmation screen was confusing - some participants thought it was something to do with them confirming that that had received something themselves.  It was suggested that it been changed to 'Ok' as this is more widely used and understood in this context.

![screenshot lost password](./img/screenshot-lost-password-sm.png)

- The 'Lost password' dialog (displayed when choosing 'I have forgotten my password') needs to be updated to explain about social backup recovery.

- When deleting a contact who is a custodian, the 'are you sure?' dialog does not explicitly warn you that deleting this contact will mean your social backup will be incomplete.

## Proposed feature improvements

[Full list of possible improvements from both sessions](./possible-improvements.md)

### Confirmation of backup piece receipt 

![](./img/social-backup-confirmation-sm.png)

Participants wanted to be sure their backup pieces had arrived with their custodians. 4 of 5 participants were able to do this by looking at the conversation with each custodian, and checking the icons next to the notification that a backup piece has been sent. A clock icon means not yet sent, one tick icon means sent and two ticks mean confirmed as received by the contact. This is similar to the interface in popular mobile messaging apps. (add screenshot). The custodians appear at the top of the contact list screen, but it was noted that it would be more convenient to see an overview of confirming reception of backup pieces rather than having to check each custodian individually. 

### Being able to change the group of trusted contacts

3 participants said it would be desirable to be able to change which contacts were custodians, or create a new backup with a different set of custodians.

## Bugs noted

### Message delivery after recovery

Currently, after recovery, messages to existing contacts are never delivered.
This issue was known before the test session and a fix is planned.

### Contact deletion

When deleting contacts, the contact is not always removed from the contact list following a confirmation of deletion. In one case, Briar crashed when deleting a contact.

### Mismatched shards

If shards from two different sets are given to the secret owner, it is impossible to recover the account.
There is a danger that a custodian chooses the wrong contact before selecting 'help recover account', meaning shards from incompatible sets get muddled together.  With the current implementation, once the secret owner has a single shard from the wrong set, they are unable to recover their account no matter how many shards from the correct set they have.

This became an issue during the testing session, after several rounds of trying the backup and recovery process,some participants had more than one contact for a single person in their contacts list.  This is a familiar situation - when somebody gets a new phone number and you often end up storing two contacts in your phone for the same person, and cant remember which one is the current one when you want to call them.

As well as fixing this issue so that extra shards from mismatched sets are ignored, it was suggested that we could send the contact name together with the shard data, and display it to the secret owner, making it clearer to them if the wrong contact had been chosen.  This needs consideration as to whether it effects security.

## Issues using a tablet rather than smartphone

The tablet used was a Huewei Mediapad AGS2-L09 running Android 8.0.0.

There were no additional issues regarding the user interface layout on a bigger display.

There was a difficulty when adding contacts with Briar's 'add contact nearby' feature, which involves a QR code scan and a handshake over wifi or bluetooth. An issue with Briar has been created (link to it).

There was also a bug with returning shards when in the role of the custodian (giving the shard rather than receiving it).

## Photos

![](./img/user-testing1.jpg)

![](./img/user-testing2.jpg)
