# Social backup feature test walk-through

Using this feature requires at least 3 briar users, who take particular roles in the process: One person is the 'secret owner' and the others are all 'custodians'.

The secret owner makes a social backup, looses their phone, gets a new one, and recovers their account with help from their custodians.

**Reminder:**  The aim is to test the social backup feature - not to get general feedback about usability of briar

### Install package on phones of secret owner and at least two custodians

Install the APK of the social backup debug build:

![QR code](./img/qr-apk.png)

https://darkcrystal.pw/assets/apk/briar-android-dark-crystal-debug.apk

Your browser should ask if you want to download the file and when opening it you may have to allow permissions to agree to installing apps from that source.

If you get a message like 'File cannot be opened' without asking about permissions, go to the device settings, search for 'Unknown' and there should be an option called something like 'Install apps from unknown sources'.

Also, with some android versions, Chrome will not let you open the APK, but if you find the downloaded file using the stock file manager app, you can open it from there.

### Set up briar

- Start Briar
- Choose 'Create new account'
- Choose a name and password
- Connect to local wifi network

### The secret owner and each custodian add each other as briar contacts 

On the secret owner, as well as with each custodian:

- On the contacts screen, choose 'add' (the plus sign on bottom right), followed by 'Add nearby'
- Agree to permissions questions
- Scan each other's QR codes.
- Wait for contact to be added.
(if this bit doesn't work for some reason, you can also 'add contact from a distance', which involves copying keys and sending them by some other means).

### The secret owner makes a social backup

![](./img/Screenshot_threshold-sm.png)

Once the secret owner has at least 2 custodians added as contacts, they do the following:

- Open menu (hamburger icon at top left of contacts screen)
- Choose 'Settings' and then 'Social Backup'
- Choose the custodians using the check boxes
- Choose the threshold (if using only 2 or 3 custodians, there will be no choice)
- Click the tick icon in top right corner.

![](./img/Screenshot_backup-pieces-sent-sm.png)

You should see a message to show it worked, and the custodians should get a notification that they have received a message.

### The secret owner looses their phone

Now lets imagine the secret owner looses their phone and gets a new one.  We can do this be clearing the app data.  The process to do this varies depending on android version, but it is something like:
- Close Briar
- Open 'Settings'
- Choose 'Apps and notifications'
- Choose the briar app 'Briar SB Debug'
- Choose 'Storage and cache'
- Choose 'Clear storage'
- Click 'ok' to confirm

### The secret owner restores their account

Now open Briar again, as though for the first time:
- Choose 'Restore account from backup'
- Click 'Begin'

![](./img/Screenshot_explainer-owner-sm.png)

One of the custodians does the following:
- Choose the secret owner from their contacts list, to open the conversation screen.
- Choose the menu icon, with three dots in the top right corner of the screen
- Choose 'Help recover account'
- Click 'Scan code'

![](./img/Screenshot_scan-sm.png)

The secret owner clicks 'Show QR code' and a QR code is displayed which the custodian must scan.

If the transmission was successful, both secret owner and custodian should see a message to say the backup piece was sent/received.  If something went wrong, an error message should be displayed and they can try another time.

![](./img/Screenshot_account-recovered-sm.png)

Once enough backup pieces are recovered, a success message should be displayed, and you will be asked to set a new password.

The account should then be restored along with the original contacts list.  You should be taken to the 'Contacts' screen, as when opening briar normally.

## Reporting bugs

If Briar crashes, you should be given the option to send a crash report.  Please do send a crash report.

If Briar does not crash but there are some problems, you can send a feedback report which contains logs which will help diagnose the problem. To do this:

- Open the main menu on the contacts screen by tapping the hamburger icon
- Choose 'Settings'
- Choose 'Send feedback'
- In the 'enter your feedback field' - put the words 'dark crystal' so we know this is in relation to this feature and not briar generally.
- Tick the 'Include anonymous data about this device' checkbox
- Tap the send icon on the top right of the screen

## Known issues

- When recovering, recovered shards are not stored on disk.  This means that if you close the app during recovery, you will loose the shards you have already recovered and need to collect them again.  This will be fixed in the second development iteration.
- After having recovered, the contacts list is restored but messages to them do not get sent.  This is because after restoring the contacts we need to do a handshake to re-establish rotation keys.  The implementation of this is currently work-in-progress. 
- When recovering, sometimes the shard transfer does not work.  Usually trying a second time works. This will hopefully be fixed soon.
