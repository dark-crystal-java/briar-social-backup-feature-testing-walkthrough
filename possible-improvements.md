
## Social Backup possible improvements

This is a collection of possible improvements based on feedback from the user testing sessions.  Have ranked them by importance divided by estimated time needed and are implementing as many as we can in the timespan of our second development iteration.

### Back end
1. [x] Fix bug with deleting contacts
2. [wip] Transport key exchange after recovery
3. [x] Failure to recover on mismatched shards
4. [x] Persistent storage of returned shards
5. [x] Improve error handling during shard return
6. [x] Improve handling of invalid/corrupt shard messages
7. [ ] Shard return over Bluetooth
8. Remote shard return - not possible in the next development round

### UI
1. [x] Change 'Got it' button used on confirmation screens
2. [x] Change wording on shard sent confirmations (remove word shard)
3. [x] Disable 'help recover account' menu option for contacts who have not sent you a shard
4. [x] Improve 'Lost password' dialog.
5. [x] When setting up a backup, if you have less than two contacts, do not display the contact selector screen as it is impossible to choose enough contacts
6. [ ] Initial explainer screen on setup before choosing custodians
7. [x] Improve screen showing existing social backup
8. [ ] Add delivery confirmation of backup pieces to existing social backup screen
8. [x] Generally improve explanations on explainer screens 
9. [ ] Notification/visualisation when recovering shards held for others
10. [ ] Add an extra warning when deleting a contact who is a custodian
11. Notification on backup updates - decided against this
12. [ ] Improve visualisation of threshold settings
13. Make 'help recover account' an option in settings menu - decided against this

## Improvements for social backup following second testing sessions

### Startup screen - make 'restore account' less prominent

In most cases the user will want to create a new account, and making them choose options as the very first thing they see creates confusion.

#### Before and after
![Screenshot welcome screen old](./img/screenshot-welcome-old.png)
![Screenshot welcome screen](./img/screenshot-welcome-screen.png)

- [x] Move restore account button to enter name screen, making creating a new account the default option
- [x] Disable restore account button when a name is entered

### Choose password screen

#### Info dialog before and after
![Before](./img/screenshot-password-info-dialog-old.png)
![After](./img/screenshot-password-info-dialog-new.png)

- [x] Improve info explainer
- [ ] Improve lost password dialogue
- General Briar issue - button to open info dialog is not prominent and likely to be overlooked - possible solution: tooltips, user on-boarding.

### Social backup setup process unclear - add explainer
![Screenshot social backup explainer](./img/screenshot-social-backup-explainer.png)
- [x] Change setup process to use view model
- [x] Implement explainer
- [x] Explainer text - what gets backed up, how many contacts is a good idea
- [x] Explainer image

### Contacts selection screen

- [ ] Improve position of confirm button when selecting contacts
- [ ] Fix issues with dark mode when selecting contacts
- [x] Set maximum number of contacts when selecting contacts

### Threshold screen

#### Before and after

![Screenshot threshold screen old](./img/Screenshot_threshold-sm.png)
![Screenshot threshold screen](./img/screenshot-select-threshold.png)

- [x] Improve terminology - term 'threshold' is technical and does not translate well
- [x] Improve visual feedback - add custodian icons
- [x] Improve visual feedback - add 'strength meter' (like when choosing a password)
- [x] Same information is repeated (improve text)
- [x] Make it clearer what it is for
- Replace slider with automatically chosen value? Decided against this

### Successful setup screen

#### Before and after
![Screenshot success old](./img/Screenshot_backup-pieces-sent-sm.png)
![Screenshot popup success](./img/screenshot-popup-success.png)

- [x] Successful setup - pop-up notification, not full screen fragment

### Guidance on first use

![Screenshot onboarding](./img/screenshot-onboarding-social-backup.png)

- [x] Onboarding help on first use - 'Material tap target prompt'

### Returning shards

<!-- TODO screenshot -->

- [x] Shard return for custodian - success should be pop-up dialogue not full screen fragment

### Existing backup screen

This screen is show when choosing the 'Social Backup' settings option when a social backup is already set up.

![Screenshot existing backup](./img/screenshot-existing-backup.png)

- [x] Make clear who custodians are, and what how many are needed to recover

## What we ***didn't*** do

### Consent for custodians

In the testing session, one participant said they expected to be asked whether they wanted to accept a backup piece, and in was noted that this is a desired feature. This is something we have given much consideration to when developing the key backup protocol, and it is a complex issue with no perfect solution.

There are two possible ways consent for social backup could be implemented, which we call strong consent and weak consent. 

![weak consent](https://darkcrystal.pw/assets/consent-mechanism-weak.png)

With **weak consent**, the secret owner sends the shard, and if the custodian chooses to reject it, it is deleted and the secret owner is informed. This makes things very simple in the 'happy case' where everyone accepts, but if a custodian rejects, things get complicated. The secret owner now needs to send a new share set to everyone, and things get messy if some custodians try to return shards from the original set. Worse still, the secret owner can never be sure the share was really deleted, which presents a security issue.

![strong consent](https://darkcrystal.pw/assets/consent-mechanism-strong.png)

With **strong consent**, the secret owner send no shards until they have received an acceptance message from all custodians. This is better for both security and for offering genuine 'opt-in' consent to the custodians. However, until all these acceptance messages are receive there is no backup in place whatsoever, and in practice we have found it often takes a long time to get responses for everyone, particularly with mobile apps where not everyone is always connected or signed in. It also presents a usability issue to the secret owner, as they will not immediately see confirmation that the backup is set up. 

For these reasons we decided not to implement consent for receiving backup pieces. These issues are discussed in more detail on our [worked example on our website](https://darkcrystal.pw/worked-example/) under the heading 'Gaining Consent from Custodians'.

### Merging 'Social Backup' and 'Remote Wipe' into one feature

Another issue that has come up is that the setup process for social backup and remote wipe is very similar, and rather than requiring the user to go through this process twice, it might make sense to have a single set of trusted contacts who act as both backup custodians and remote wipers. This would reduce the cognitive load of users, and make it more likely that both features are used.

However, in our first set of user testing sessions, we discussed with participants whether there were some cases where they would want somebody to be a backup custodian but not a remote wiper, or vice-versa. Participants said that for social backup they would choose people in a 'stable situation' such as older relatives, who were unlikely to loose their device, and that for remote wipe they would rather choose people with whom they have most regular contact and who are most involved in their activities, as they were likely to know sooner when they are in danger.

There are also issues relating to the relationship between the trusted contacts. With Remote Wipe, it is important that the wipers know who each other are, as they may need to inform each other of the situation, or discuss whether activating a wipe is appropriate if they are unsure.  With Social Backup there is a great security advantage in custodians not knowing who each other are, as an adversary cannot coerce them into giving up the identities of the others. This is another argument to not merge the two support groups. 

We discuss this issue further in our [final report](https://gitlab.com/dark-crystal-java/dark-crystal-final-report/-/blob/main/report.pdf)
